-- Databricks notebook source
CREATE DATABASE IF NOT EXISTS formula1_presentation
LOCATION "/mnt/formula1bwt/presentation"

-- COMMAND ----------

desc database  formula1_presentation

-- COMMAND ----------

show databases

-- COMMAND ----------

-- MAGIC %python
-- MAGIC display(dbutils.fs.ls("/mnt/formula1bwt/presentation"))

-- COMMAND ----------


