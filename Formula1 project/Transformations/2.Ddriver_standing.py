# Databricks notebook source
# MAGIC %md
# MAGIC ##produce Driver standing 

# COMMAND ----------

# MAGIC  %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

race_result_df = spark.read.parquet(f"{presentation_folder_path}/race_results")

# COMMAND ----------

race_result_df.columns

# COMMAND ----------

from pyspark.sql.functions import sum,col,count,when,desc,rank
from pyspark.sql.window import Window

# COMMAND ----------

driver_standng_df = race_result_df.withColumn("points",col("points").cast("int")).groupBy("race_year","driver_name","driver_nationality","team").agg(sum("points").alias("total_point"),count(when(col("position")==1,True)).alias("wins"))

# COMMAND ----------

driver_standng_df.filter("race_year == 2020")

# COMMAND ----------

window_spase = Window.partitionBy("race_year").orderBy(desc("total_point"),desc("wins"))
final_df = driver_standng_df.withColumn("rank",rank().over(window_spase))

# COMMAND ----------

display(final_df.filter("race_year == 2020"))

# COMMAND ----------

final_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_presentation.driver_standing")


# COMMAND ----------

final_df.write.mode("overwrite").parquet(f"{presentation_folder_path}/driver_standing")

# COMMAND ----------


