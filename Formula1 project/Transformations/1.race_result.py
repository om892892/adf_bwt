# Databricks notebook source
# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

from pyspark.sql.functions import col

# COMMAND ----------

races_df = spark.read.parquet(f"{processed_folder_path}/races").withColumnRenamed("name","race_name").withColumnRenamed("race_timestamp","race_date")

# COMMAND ----------

circuits_df = spark.read.parquet(f"{processed_folder_path}/circuits").withColumnRenamed("location","circuit_location")


# COMMAND ----------

driver_df = spark.read.parquet(f"{processed_folder_path}/drivers").withColumnRenamed("number","driver_number").withColumnRenamed("name","driver_name").withColumnRenamed("nationality","driver_nationality")



# COMMAND ----------

constructors_df = spark.read.parquet(f"{processed_folder_path}/constructors").withColumnRenamed("name","team")
constructors_df.columns 

# COMMAND ----------

result_df = spark.read.parquet(f"{processed_folder_path}/results").withColumnRenamed("time","race_time")


# COMMAND ----------

# MAGIC %md
# MAGIC ##Join all tables

# COMMAND ----------

race_circuits_df = races_df.join(circuits_df,races_df.circuit_id == circuits_df.circuit_id,"inner").select("race_id","race_year","race_name","race_date","circuit_location")

# COMMAND ----------

race_join_df = result_df.join(race_circuits_df,result_df.race_id == race_circuits_df.race_id)\
                              .join(driver_df,driver_df.driver_id == result_df.driver_id)\
                              .join(constructors_df,constructors_df.constructor_id == result_df.constructor_id)\
                                
            

# COMMAND ----------

final_df = race_join_df.select("race_year","race_name","race_date","circuit_location","driver_name","driver_number","driver_nationality","team","grid","fastest_lap","race_time","points","position").withColumn("created_date",current_timestamp())

# COMMAND ----------

final_df.filter("race_year == 2020 and race_name == 'Abu Dhabi Grand Prix'").orderBy(col("points").desc())

# COMMAND ----------

final_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_presentation.race_results")

# COMMAND ----------

final_df.write.mode("overwrite").parquet(f"{presentation_folder_path}/race_results")

# COMMAND ----------


