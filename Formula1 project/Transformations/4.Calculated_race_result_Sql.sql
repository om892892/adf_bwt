-- Databricks notebook source
USE formula1_processed;

-- COMMAND ----------

show tables in formula1_processed

-- COMMAND ----------

"race_year","race_name","race_date","circuit_location","driver_name","driver_number","driver_nationality","team","grid","fastest_lap","race_time","points","position"

-- COMMAND ----------

CREATE TABLE formula1_presentation.Calculated_race_results
USING parquet 
AS
SELECT races.race_year ,
      constructors.name as team_name,  
      drivers.name as drivers_name,
      results.position,
      results.points,
      11- results.position as calculated_point
 FROM results 
  JOIN formula1_processed.drivers ON (results.driver_id = drivers.driver_id)
  JOIN formula1_processed.constructors ON (results.constructor_id = constructors.constructor_id)
  JOIN formula1_processed.races ON (results.race_id = races.race_id)

  where results.position <= 10

-- COMMAND ----------

select * from formula1_presentation.Calculated_race_results

-- COMMAND ----------


