# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest drivers.json File

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_data_value","")
v_data_value = dbutils.widgets.get("p_data_value")

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC
# MAGIC %run "../includes/common_function/"

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

from pyspark.sql.types import StringType,IntegerType,DoubleType,StructType,StructField,DateType
from pyspark.sql.functions import current_timestamp,concat,col,lit

# COMMAND ----------

#name has two nested columns ( forename, surname)
name_schema = StructType ([
                            StructField("forename",StringType()),\
                            StructField("surname",StringType())
])

# COMMAND ----------

# Get Schema
drivers_schema = StructType ([ StructField("driverId",IntegerType()),\
                                    StructField("driverRef",StringType()),\
                                    StructField("number",IntegerType()),\
                                    StructField("code",StringType()),\
                                    StructField("name",name_schema),\
                                    StructField("dob",DateType()),\
                                    StructField("nationality",StringType()),\
                                    StructField("url",StringType())
                ])

# COMMAND ----------

# Read the JSON file with Schema
drivers_df = spark.read.json(f"{raw_folder_path}/{v_file_date}/drivers.json",schema=drivers_schema)
drivers_df.display()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Rename columns and new columns
# MAGIC 1. driver renamed driver_id 
# MAGIC 2. driverRef renamed to driver_ref
# MAGIC 3. ingestion date added
# MAGIC 4. name added with concatenation of forename and surname 
# MAGIC

# COMMAND ----------

#Rename the column and ingestion 
drivers_renamed_df = drivers_df.withColumnRenamed("driverId", 'driver_id')\
                                         .withColumnRenamed("driverRef","driver_ref")\
                                         .withColumn("ingestion_date",current_timestamp())\
                                         .withColumn("name",concat(col("name.forename"),lit(" "),"name.surname"))\
                                         .withColumn("data_source",lit(v_data_value))\
                                         .withColumn("file_date",lit(v_file_date))

# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-3 Drop the unwanted columna
# MAGIC 1. name.forename, name.surname , url 

# COMMAND ----------

#select required columns
drivers_final_df = drivers_renamed_df.drop("url")


# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-4 Write the file in parquect 

# COMMAND ----------

drivers_final_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_processed.drivers")

# COMMAND ----------

#write the file 
drivers_final_df.write.mode("overwrite").parquet(f"{processed_folder_path}/drivers")

# COMMAND ----------

display(spark.read.parquet(f"{processed_folder_path}/drivers"))

# COMMAND ----------

dbutils.notebook.exit("Success")
