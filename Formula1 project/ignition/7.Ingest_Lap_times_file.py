# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest lap_times.json File

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

from pyspark.sql.types import StringType,IntegerType,StructType,StructField
from pyspark.sql.functions import current_timestamp

# COMMAND ----------

# Get Schema
lap_times_schema = StructType ([  StructField("raceId",IntegerType()),\
                                StructField("driverId",IntegerType()),\
                                StructField("lap",IntegerType()),\
                                StructField("positions",IntegerType()),\
                                StructField("time",StringType()),\
                                StructField("milliseconds",IntegerType()),\
                                
                              
])

# COMMAND ----------

# Read the JSON file with Schema
lap_times_df = spark.read.csv(f"{raw_folder_path}/lap_times/lap_times_split*",schema=lap_times_schema)


# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Rename columns and new columns
# MAGIC 1. raceid renamed race_id 
# MAGIC 2. driverId renamed to driver_id
# MAGIC 3. ingestion date added
# MAGIC

# COMMAND ----------

#Rename the column and ingestion 
lap_times_renamed_df = lap_times_df.withColumnRenamed("raceId", 'race_id')\
                                .withColumnRenamed("driverId","driver_id")\
                                .withColumn("ingestion_date",current_timestamp())


# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-4 Write the file in parquect 

# COMMAND ----------

lap_times_renamed_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_processed.lap_times")

# COMMAND ----------

#write the file 
lap_times_renamed_df.write.mode("overwrite").parquet(f"{processed_folder_path}/lap_times")

# COMMAND ----------

display(spark.read.parquet("/mnt/formula1bwt/processed/lap_times"))

# COMMAND ----------

dbutils.notebook.exit("Success")
