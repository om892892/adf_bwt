# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest results.json File

# COMMAND ----------

spark.read.json("/mnt/formula1bwt/raw/2021-03-21/results.json").createOrReplaceTempView("results_cutover")

# COMMAND ----------

# MAGIC %sql
# MAGIC select raceId,count(1) from results_cutover
# MAGIC group by raceId
# MAGIC order by raceId desc

# COMMAND ----------

spark.read.json("/mnt/formula1bwt/raw/2021-03-28/results.json").createOrReplaceTempView("results_w1")

# COMMAND ----------

# MAGIC %sql
# MAGIC select raceId,count(1) 
# MAGIC from results_w1
# MAGIC group by raceId
# MAGIC order by raceId desc

# COMMAND ----------

spark.read.json("/mnt/formula1bwt/raw/2021-04-18/results.json").createOrReplaceTempView("results_w2")

# COMMAND ----------

# MAGIC %sql
# MAGIC select raceId,count(1) from results_w2
# MAGIC group by raceId
# MAGIC order by raceId desc

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_data_value","")
v_data_value = dbutils.widgets.get("p_data_value")

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC
# MAGIC %run "../includes/common_function/"

# COMMAND ----------

from pyspark.sql.types import StringType,IntegerType,DoubleType,StructType,StructField,DateType,FloatType
from pyspark.sql.functions import current_timestamp,concat,col,lit

# COMMAND ----------

# Get Schema
results_schema = StructType ([  StructField("constructorId",IntegerType()),\
                                StructField("driverId",IntegerType()),\
                                StructField("fastestLap",IntegerType()),\
                                StructField("fastestLapSpeed",FloatType()),\
                                StructField("fastestLapTime",StringType()),\
                                StructField("grid",IntegerType()),\
                                StructField("laps",IntegerType()),\
                                StructField("milliseconds",IntegerType()),\
                                StructField("number",IntegerType()),\
                                StructField("points",FloatType()),\
                                StructField("position",IntegerType()),\
                                StructField("positionOrder",IntegerType()),\
                                StructField("positionText",StringType()),\
                                StructField("raceId",IntegerType()),\
                                StructField("rank",IntegerType()),\
                                StructField("resultId",IntegerType()),\
                                StructField("statusId",StringType()),\
                                StructField("time",StringType()),\
                                
                ])

# COMMAND ----------

# Read the JSON file with Schema
results_df = spark.read.json(f"{raw_folder_path}/{v_file_date}/results.json",schema=results_schema)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Rename columns and new columns
# MAGIC 1. driver renamed driver_id 
# MAGIC 2. driverRef renamed to driver_ref
# MAGIC 3. ingestion date added
# MAGIC 4. name added with concatenation of forename and surname 
# MAGIC

# COMMAND ----------

#Rename the column and ingestion 
results_renamed_df = results_df.withColumnRenamed("constructorId", 'constructor_id')\
                                .withColumnRenamed("driverId","driver_id")\
                                .withColumnRenamed("fastestLap","fastest_lap")\
                                .withColumnRenamed("fastestLapSpeed", 'fastest_lap_speed')\
                                .withColumnRenamed("fastestLapTime","fastest_lap_time")\
                                .withColumnRenamed("positionOrder","position_order")\
                                .withColumnRenamed("raceId", 'race_id')\
                                .withColumnRenamed("resultId","result_id")\
                                .withColumnRenamed("positionText","position_text")\
                                .withColumn("ingestion_date",current_timestamp())\
                                .withColumn("data_source",lit(v_data_value))\
                                .withColumn("file_date",lit(v_file_date))


# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-3 Drop the unwanted columna
# MAGIC 1.name.forename, name.surname , url 

# COMMAND ----------

#select required columns
results_final_df = results_renamed_df.drop("statusId")


# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-4 Write the file in parquect 

# COMMAND ----------

results_final_df.write.mode("append").format("parquet").saveAsTable("formula1_processed.results")

# COMMAND ----------

# MAGIC %sql
# MAGIC select race from formula1_processed.results

# COMMAND ----------

#write the file 
# results_final_df.write.mode("overwrite").parquet(f"{processed_folder_path}/results")

# COMMAND ----------

dbutils.notebook.exit("Success")
