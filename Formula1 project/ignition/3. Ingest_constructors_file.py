# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest Constructors.json File

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_data_value","")
v_data_value = dbutils.widgets.get("p_data_value")

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

from pyspark.sql.types import StringType,IntegerType,DoubleType,StructType,StructField
from pyspark.sql.functions import current_timestamp,lit

# COMMAND ----------

# Get Schema
constructors_schema = StructType ([StructField("constructorId",IntegerType()),\
                    StructField("constructorRef",StringType()),
                    StructField("name",StringType()),\
                    StructField("nationality",StringType()),\
                    StructField("url",StringType())
])

# COMMAND ----------

# Read the JSON file with Schema
constructors_df = spark.read.json(f"{raw_folder_path}/{v_file_date}/constructors.json",schema=constructors_schema)


# COMMAND ----------

# MAGIC %md
# MAGIC ### Rename the column and new column ingestion date

# COMMAND ----------

# Rename the column and ingestion
constructors_renamed_df = (
    constructors_df.withColumnRenamed("constructorId", "constructor_id")
    .withColumnRenamed("constructorRef", "constructor_ref")
    .withColumn("ingestion_date", current_timestamp())
    .withColumn("data_source", lit(v_data_value))
    .withColumn("file_date", lit(v_file_date))
)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Select required columns 

# COMMAND ----------

constructors_renamed_df.columns

# COMMAND ----------

#select required columns
constructors_final_df = constructors_renamed_df.select("constructor_id","constructor_ref","name","nationality","ingestion_date","data_source","file_date")


# COMMAND ----------

# MAGIC %md
# MAGIC ###Write the file in parquect 

# COMMAND ----------

constructors_final_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_processed.constructors")

# COMMAND ----------

#write the file 
constructors_final_df.write.mode("overwrite").parquet(f"{processed_folder_path}/constructors")

# COMMAND ----------

dbutils.notebook.exit("Success")

# COMMAND ----------

display(spark.read.parquet(f"{processed_folder_path}/constructors"))
