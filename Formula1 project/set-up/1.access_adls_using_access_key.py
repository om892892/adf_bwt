# Databricks notebook source
# MAGIC %md
# MAGIC ####Access Azure Data Lake using Access key
# MAGIC 1. Set the spark configuration fs.azure.account.kay
# MAGIC 2. List file from demo containers 
# MAGIC 3. Read data from give folder 

# COMMAND ----------

formula1_account_key = dbutils.secrets.get(scope="formula1-scope",key="formula1dl-account-key")

# COMMAND ----------

#inserted of above code you can use secrets key 
spark.conf.set("fs.azure.account.key.formula1bwt.dfs.core.windows.net",
               formula1_account_key)



# COMMAND ----------

# spark.conf.set("fs.azure.account.key.formula1bwt.dfs.core.windows.net",
#                "uUroWCfBImeXKWgwa84xhhx7CRBY/cvQsb0ouNzooyi5XTsSxRAp0RC7wa5HOJVrsnS/hs370MTB+AStCE8sKQ==")



# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula1bwt.dfs.core.windows.net")  )

# COMMAND ----------

display(spark.read.csv("abfss://demo@formula1bwt.dfs.core.windows.net/employee.csv"))

# COMMAND ----------


