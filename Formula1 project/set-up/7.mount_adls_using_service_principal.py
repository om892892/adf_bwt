# Databricks notebook source
# MAGIC %md
# MAGIC
# MAGIC ###Mount Azure Data Lake Using Services Principle
# MAGIC Stepa to follow
# MAGIC 1. Get client_id,tenant_idand client_secret from key vault 
# MAGIC 2. Set spark configwith App/Client id,Directory/Tenant id & Secret
# MAGIC 3. Call file System utlity mount to mountthe Storage
# MAGIC 4. Explore other file system utlies related to mount(list all mount , unmount )
# MAGIC

# COMMAND ----------

# you can use instead of hard-coded ,use secrets key 
client_id =formula1_sas_token = dbutils.secrets.get(scope="formula1-scope",key="formula1-demo-client-id")
tanant_id = formula1_sas_token = dbutils.secrets.get(scope="formula1-scope",key="formula1-demo-tanant-id")
clinet_secret = formula1_sas_token = dbutils.secrets.get(scope="formula1-scope",key="formula1-demo-clinet-secret")

# COMMAND ----------

# It's  gave the hard-coded valure of services principal 
# client_id = "57376fec-8238-40ba-b0ed-e39319199415"
# tanant_id = "e3deac48-8e5c-4121-8e55-9dd11eb5710d"
# clinet_secret = "fwM8Q~f370CBWJM85TXr06kXVCCFGDFXUdlAZdpS"

# COMMAND ----------

configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": client_id,
          "fs.azure.account.oauth2.client.secret":clinet_secret,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{tanant_id}/oauth2/token"}

# COMMAND ----------


# Optionally, you can add <directory-name> to the source URI of your mount point.
dbutils.fs.mount(
  source = "abfss://demo@formula1bwt.dfs.core.windows.net/",
  mount_point = "/mnt/formula1bwt/demo",
  extra_configs = configs)

# COMMAND ----------

display(dbutils.fs.ls("/mnt/formula1bwt/demo")  )

# COMMAND ----------

# display(spark.read.csv("abfss://demo@formula1bwt.dfs.core.windows.net/employee.csv"))

# To cheak the mounts files 
display(dbutils.fs.mounts())

# COMMAND ----------


