# Databricks notebook source
# MAGIC %md
# MAGIC ###Access Azure Data Lake using SAS Token
# MAGIC 1. Set the spark configuration sas token
# MAGIC 2. List file from demo containers 
# MAGIC 3. Read data from give folder 

# COMMAND ----------

formula1_sas_token = dbutils.secrets.get(scope="formula1-scope",key="formula1-demo-sas-token")

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.formula1bwt.dfs.core.windows.net", "SAS")
spark.conf.set("fs.azure.sas.token.provider.type.formula1bwt.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.sas.FixedSASTokenProvider")
spark.conf.set("fs.azure.sas.fixed.token.formula1bwt.dfs.core.windows.net",formula1_sas_token)

# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formula1bwt.dfs.core.windows.net")  )

# COMMAND ----------

display(spark.read.csv("abfss://demo@formula1bwt.dfs.core.windows.net/employee.csv",header=True))

# COMMAND ----------


