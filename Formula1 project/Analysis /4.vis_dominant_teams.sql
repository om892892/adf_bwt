-- Databricks notebook source
-- MAGIC %python 
-- MAGIC html = """<h1 style="color:black;text-align:center;font-family:Ariel">Report on Dominant Formula one Driver </h1"""
-- MAGIC
-- MAGIC displayHTML(html)

-- COMMAND ----------

CREATE OR REPLACE TEMP VIEW v_dominant_teams
AS
SELECT 
  team_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points,
  rank() OVER (ORDER BY avg(calculated_point) desc) as teams_rank
  FROM formula1_presentation.calculated_race_results
  GROUP BY team_name
  HAVING  count(1) >= 100
  ORDER BY avg_points desc

-- COMMAND ----------

SELECT race_year,
  team_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  where  team_name in ( select team_name from v_dominant_teams where  teams_rank <= 10 )
  GROUP BY race_year,team_name
  ORDER BY race_year, avg_points desc

-- COMMAND ----------

SELECT race_year,
  team_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  where  team_name in ( select team_name from v_dominant_teams where  teams_rank <= 5 )
  GROUP BY race_year,team_name
  ORDER BY race_year, avg_points desc  

-- COMMAND ----------

SELECT race_year,
  team_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  where  team_name in ( select team_name from v_dominant_teams where  teams_rank <= 10 )
  GROUP BY race_year,team_name
  ORDER BY race_year, avg_points desc
