-- Databricks notebook source
-- MAGIC %md
-- MAGIC ##Drop all tables in Database 

-- COMMAND ----------

DROP DATABASE IF EXISTS formula1_processed CASCADE; 

-- COMMAND ----------

CREATE DATABASE IF NOT EXISTS formula1_processed
LOCATION "/mnt/formula1bwt/processed";

-- COMMAND ----------

DROP DATABASE IF EXISTS formula1_presentation CASCADE; 

-- COMMAND ----------

CREATE DATABASE IF NOT EXISTS formula1_processed
LOCATION "/mnt/formula1bwt/presentation";
