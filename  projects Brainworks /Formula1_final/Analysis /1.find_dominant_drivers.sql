-- Databricks notebook source
select * from formula1_presentation.Calculated_race_results

-- COMMAND ----------

SELECT 
  drivers_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  GROUP BY drivers_name
  HAVING  count(1)  >= 50
  ORDER BY avg_points desc


-- COMMAND ----------

SELECT 
  drivers_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  WHERE race_year BETWEEN 2011 AND 2020
  GROUP BY drivers_name
  HAVING  count(1)  >= 50
  ORDER BY avg_points desc


-- COMMAND ----------


