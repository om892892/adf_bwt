-- Databricks notebook source
CREATE OR REPLACE TEMP VIEW v_dominant_driver
AS
SELECT 
  drivers_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points,
  rank() OVER (ORDER BY avg(calculated_point) desc) as driver_rank
  FROM formula1_presentation.calculated_race_results
  GROUP BY drivers_name
  HAVING  count(1)  >= 50
  ORDER BY avg_points desc

-- COMMAND ----------

SELECT race_year
  drivers_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  GROUP BY race_year,drivers_name
  ORDER BY race_year, avg_points desc

-- COMMAND ----------

SELECT race_year,
  drivers_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  where  drivers_name in ( select drivers_name from v_dominant_driver where  driver_rank <= 10 )
  GROUP BY race_year,drivers_name
  ORDER BY race_year, avg_points desc

-- COMMAND ----------

SELECT race_year,
  drivers_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  where  drivers_name in ( select drivers_name from v_dominant_driver where  driver_rank <= 10 )
  GROUP BY race_year,drivers_name
  ORDER BY race_year, avg_points desc

-- COMMAND ----------

SELECT race_year,
  drivers_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  where  drivers_name in ( select drivers_name from v_dominant_driver where  driver_rank <= 10 )
  GROUP BY race_year,drivers_name
  ORDER BY race_year, avg_points desc
