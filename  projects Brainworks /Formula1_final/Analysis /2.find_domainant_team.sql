-- Databricks notebook source
-- MAGIC %md
-- MAGIC ## Performance  of teams

-- COMMAND ----------

SELECT 
  team_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  GROUP BY team_name
  HAVING  count(1)  >= 100
  ORDER BY avg_points desc


-- COMMAND ----------

SELECT 
  team_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  WHERE race_year BETWEEN 2011 and 2021
  GROUP BY team_name
  HAVING  count(1)  >= 100
  ORDER BY avg_points desc

-- COMMAND ----------

SELECT 
  team_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  WHERE race_year BETWEEN 2001 and 2011
  GROUP BY team_name
  HAVING  count(1)  >= 100
  ORDER BY avg_points desc

-- COMMAND ----------

SELECT 
  team_name,
  count(1) as total_races,
  sum(calculated_point) as total_point,
  avg(calculated_point) as avg_points
  FROM formula1_presentation.calculated_race_results
  WHERE race_year BETWEEN 1900 and 1999
  GROUP BY team_name
  HAVING  count(1)  >= 100
  ORDER BY avg_points desc

-- COMMAND ----------


