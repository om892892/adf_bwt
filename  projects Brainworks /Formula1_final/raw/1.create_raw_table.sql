-- Databricks notebook source
-- MAGIC %md
-- MAGIC ###Create Database

-- COMMAND ----------

create database if not exists formulaa1_raw;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ##Create Table with CSV files 

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ####1.Create Table Circuits

-- COMMAND ----------

drop table if exists formulaa1_raw.circuits;
CREATE TABLE IF NOT EXISTS formulaa1_raw.circuits(
circuitId INT,
circuitRef STRING,
name STRING,
location STRING,
country STRING,
lat DOUBLE,
lng DOUBLE,
alt INT,
url STRING
)
using csv
options (path "/mnt/formulaa1bwt/raw/circuits.csv",header=True)


-- COMMAND ----------

select * from formulaa1_raw.circuits

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ###2.Create table races
-- MAGIC

-- COMMAND ----------

CREATE TABLE IF NOT EXISTS formulaa1_raw.races(
raceId  integer,
 year  integer,
 round  integer,
 circuitId  integer,
 name  string,
 date  date,
 time  string,
 url  string
)
using csv
options (path "/mnt/formulaa1bwt/raw/races.csv",header=True) 

-- COMMAND ----------

select * from formulaa1_raw.races

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #Create Table with JSON files

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #####3.Create Table Constructors
-- MAGIC  1. Single Line JSON
-- MAGIC  2. Simple Structure 

-- COMMAND ----------

drop table if exists formulaa1_raw.constructors;

CREATE TABLE IF NOT EXISTS formulaa1_raw.constructors 
(
  constructorId INT,
constructorRef STRING ,
name STRING ,
nationality STRING ,
url STRING 
)
USING json
OPTIONS  (path "/mnt/formulaa1bwt/raw/constructors.json")

-- COMMAND ----------

select * from formulaa1_raw.constructors

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #####4.Create Table Driver
-- MAGIC  1. Single Line JSON
-- MAGIC  2. Complex Structure 

-- COMMAND ----------

DROP TABLE IF EXISTS formulaa1_raw.drivers;
CREATE TABLE IF NOT EXISTS formulaa1_raw.drivers
(
  driverId INT ,
driverRef STRING ,
number INT ,
code STRING ,
name STRUCT <forname: STRING,surname: STRING>,
dob date,
nationality STRING ,
url STRING 
)
USING JSON
OPTIONS (path "/mnt/formulaa1bwt/raw/drivers.json")

-- COMMAND ----------

select * from formulaa1_raw.drivers

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ####5.Create Table Result
-- MAGIC  1. Single Line JSON
-- MAGIC  2. Simple Structure 

-- COMMAND ----------

DROP TABLE IF EXISTS formulaa1_raw.results;
CREATE TABLE IF NOT EXISTS formulaa1_raw.results(
resultId INT,
raceId INT,
driverId INT,
constructorId INT,
number INT,
grid INT,
position INT,
positionText STRING,
positionOrder INT,
points INT,
laps INT,
time STRING,
milliseconds INT,
fastestLap INT,
rank INT,
fastestLapTime STRING,
fastestLapSpeed float,
statusId STRING
)
USING json
OPTIONS (path "/mnt/formulaa1bwt/raw/results.json")

-- COMMAND ----------

select * from formulaa1_raw.results

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ####6.Create Table Pitstop
-- MAGIC  1. Multi Line JSON
-- MAGIC  2. Simple Structure 

-- COMMAND ----------

DROP TABLE IF EXISTS formulaa1_raw.pit_stops;
CREATE TABLE IF NOT EXISTS formulaa1_raw.pit_stops(
  driverId INT ,
duration STRING ,
lap INT ,
milliseconds INT ,
raceId INT ,
stop STRING ,
time STRING 
)
USING JSON
OPTIONS (path "/mnt/formulaa1bwt/raw/pit_stops.json",multiLine  True)

-- COMMAND ----------

SELECT * FROM formulaa1_raw.pit_stops

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ####7.Create Table Lap Times
-- MAGIC  1. CSV file
-- MAGIC  2. Multi files 

-- COMMAND ----------

DROP TABLE IF EXISTS formulaa1_raw.lap_times;
CREATE TABLE IF NOT EXISTS formulaa1_raw.lap_times(
raceId INT,
driverId INT,
lap INT,
positions INT,
time STRING,
milliseconds INT
)
USING csv
OPTIONS (path "/mnt/formulaa1bwt/raw/lap_times/")

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ####8.Create Table qualifying
-- MAGIC  1.  JSON file
-- MAGIC  2. Multi Line JSON

-- COMMAND ----------

DROP TABLE IF EXISTS formulaa1_raw.qualifying;
CREATE TABLE IF NOT EXISTS formulaa1_raw.qualifying(
constructorId INT ,
driverId INT ,
number INT ,
position INT ,
q1 STRING ,
q2 STRING ,
q3 STRING,
qualifyId INT ,
raceId INT 
 )
USING JSON
OPTIONS (path "/mnt/formulaa1bwt/raw/qualifying/qualifying_split*",multiLine  True)

-- COMMAND ----------

-- MAGIC %python
-- MAGIC #All tables 
-- MAGIC #SELECT * FROM formulaa1_processed.constructors; 
-- MAGIC # SELECT * FROM formulaa1_processed.circuits; 
-- MAGIC # SELECT * FROM formulaa1_processed.drivers; 
-- MAGIC # SELECT * FROM formulaa1_processed.lap_times; 
-- MAGIC # SELECT * FROM formulaa1_processed.pit_stops; 
-- MAGIC # SELECT * FROM formulaa1_processed.qualifying; 
-- MAGIC # SELECT * FROM formulaa1_processed.races; 
-- MAGIC # SELECT * FROM formulaa1_processed.results
