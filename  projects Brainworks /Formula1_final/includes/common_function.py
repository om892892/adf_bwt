# Databricks notebook source
from pyspark.sql.window import Window
from pyspark.sql.functions import lit,current_timestamp,col,concat,desc,rank

# COMMAND ----------

from pyspark.sql.functions import current_timestamp

def add_ingestion_date(input_df):
    output_df = input_df.withColumn(" ingestion_date",current_timestamp())
    return output_df


# COMMAND ----------

def re_arrange_partition_column(input_df, partition_column):
    column_list = []
    for column_name in input_df.columns:
        if column_name != partition_column:
            column_list.append(column_name)
    column_list.append(partition_column)
    output_df = input_df.select(column_list)
    return output_df

# COMMAND ----------

def overwrite_partition(input_df,db_name,table_name,partitioncol_name):
    output_column_df = re_arrange_partition_column(input_df,partitioncol_name)
    spark.conf.set("spark.sql.sources.partitionOverwriteMode","dynamic")
    if (spark._jsparkSession.catalog().tableExists(f"{db_name}.{table_name}")):
        output_column_df.write.mode("overwrite").insertInto(f"{db_name}.{table_name}")
    else:
        output_column_df.write.mode("overwrite").partitionBy(partitioncol_name).format("parquet").saveAsTable(f"{db_name}.{table_name}")

