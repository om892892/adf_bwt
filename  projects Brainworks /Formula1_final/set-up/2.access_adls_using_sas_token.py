# Databricks notebook source
# MAGIC %md
# MAGIC ###Access Azure Data Lake using SAS Token
# MAGIC 1. Set the spark configuration sas token
# MAGIC 2. List file from demo containers 
# MAGIC 3. Read data from give folder 

# COMMAND ----------

formula1_sas_token = dbutils.secrets.get(scope="formula1-scope",key="formula1-demo-sas-token1")

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.formulaa1bwt.dfs.core.windows.net", "SAS")
spark.conf.set("fs.azure.sas.token.provider.type.formulaa1bwt.dfs.core.windows.net", "org.apache.hadoop.fs.azurebfs.sas.FixedSASTokenProvider")
spark.conf.set("fs.azure.sas.fixed.token.formulaa1bwt.dfs.core.windows.net",formula1_sas_token)

# COMMAND ----------

#sas_token ="sp=r&st=2024-04-20T17:55:29Z&se=2024-06-05T01:55:29Z&spr=https&sv=2022-11-02&sr=c&sig=xON1nAl4FJOjfbIF0dUiB3OvOaQELzaNs1AJHW7%2FZAw%3D"

sas_token = "sp=rl&st=2024-04-20T18:26:19Z&se=2024-05-31T02:26:19Z&spr=https&sv=2022-11-02&sr=c&sig=ppaMeg%2BqkklkAKtOdewngJTumcV35pXP2hke2AoxFNs%3D"

# COMMAND ----------

display(dbutils.fs.ls("abfss://demo@formulaa1bwt.dfs.core.windows.net"))

# COMMAND ----------

display(spark.read.csv("abfss://demo@formulaa1bwt.dfs.core.windows.net/circuits.csv",header=True))

# COMMAND ----------


