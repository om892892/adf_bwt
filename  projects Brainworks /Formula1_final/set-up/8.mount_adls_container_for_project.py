# Databricks notebook source
# MAGIC %md
# MAGIC
# MAGIC ###Mount Azure Data Lake Container for Project
# MAGIC

# COMMAND ----------

def mount_adls(storage_account_name, container_name ):
    # Get secrets from  key vault    
# you can use instead of hard-coded ,use secrets key 
    client_id =formula1_sas_token = dbutils.secrets.get(scope="formula1-scope",key="formula1-demo-client-id")
    tanant_id = formula1_sas_token = dbutils.secrets.get(scope="formula1-scope",key="formula1-demo-tanant-id")
    clinet_secret = formula1_sas_token = dbutils.secrets.get(scope="formula1-scope",key="formula1-demo-clinet-secret1")
    
    # Set spark config
    configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": client_id,
          "fs.azure.account.oauth2.client.secret":clinet_secret,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{tanant_id}/oauth2/token"}
    
    #Unmount the mount point if already exists   
    if any(mount.mountPoint == f"/mmnt/{storage_account_name}/{container_name}" for mount in dbutils.fs.mounts()):
        dbutils.fs.unmount(f"/mnt/{storage_account_name}/{container_name}")
    
    # Optionally, you can add <directory-name> to the source URI of your mount point.
    dbutils.fs.mount(
        source = f"abfss://{container_name}@{storage_account_name}.dfs.core.windows.net/",
        mount_point = f"/mnt/{storage_account_name}/{container_name}",
        extra_configs = configs)
        
    # To cheak the mounts files 
    display(dbutils.fs.mounts())

# COMMAND ----------

# storage_account_name and container_name call the function
mount_adls('formulaa1bwt','raw')

# COMMAND ----------

# storage_account_name and container_name call the function
mount_adls('formulaa1bwt','presentation')

# COMMAND ----------

# storage_account_name and container call the function
mount_adls('formulaa1bwt','processed')
