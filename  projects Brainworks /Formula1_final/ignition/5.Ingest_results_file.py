# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest results.json File
# MAGIC
# MAGIC ### Incremental Load

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_data_value","")
v_data_value = dbutils.widgets.get("p_data_value")

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC
# MAGIC %run "../includes/common_function/"

# COMMAND ----------

from pyspark.sql.types import StringType,IntegerType,DoubleType,StructType,StructField,DateType,FloatType
from pyspark.sql.functions import current_timestamp,concat,col,lit

# COMMAND ----------

# Get Schema
results_schema = StructType ([  StructField("constructorId",IntegerType()),\
                                StructField("driverId",IntegerType()),\
                                StructField("fastestLap",IntegerType()),\
                                StructField("fastestLapSpeed",FloatType()),\
                                StructField("fastestLapTime",StringType()),\
                                StructField("grid",IntegerType()),\
                                StructField("laps",IntegerType()),\
                                StructField("milliseconds",IntegerType()),\
                                StructField("number",IntegerType()),\
                                StructField("points",FloatType()),\
                                StructField("position",IntegerType()),\
                                StructField("positionOrder",IntegerType()),\
                                StructField("positionText",StringType()),\
                                StructField("raceId",IntegerType()),\
                                StructField("rank",IntegerType()),\
                                StructField("resultId",IntegerType()),\
                                StructField("statusId",StringType()),\
                                StructField("time",StringType()),\
                                
                ])

# COMMAND ----------

# Read the JSON file with Schema
results_df = spark.read.json(f"{raw_folder_path}/{v_file_date}/results.json",schema=results_schema)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Rename columns and new columns
# MAGIC 1. driver renamed driver_id 
# MAGIC 2. driverRef renamed to driver_ref
# MAGIC 3. ingestion date added
# MAGIC 4. name added with concatenation of forename and surname 
# MAGIC

# COMMAND ----------

#Rename the column and ingestion 
results_renamed_df = results_df.withColumnRenamed("constructorId", 'constructor_id')\
                                .withColumnRenamed("driverId","driver_id")\
                                .withColumnRenamed("fastestLap","fastest_lap")\
                                .withColumnRenamed("fastestLapSpeed", 'fastest_lap_speed')\
                                .withColumnRenamed("fastestLapTime","fastest_lap_time")\
                                .withColumnRenamed("positionOrder","position_order")\
                                .withColumnRenamed("raceId", 'race_id')\
                                .withColumnRenamed("resultId","result_id")\
                                .withColumnRenamed("positionText","position_text")\
                                .withColumn("ingestion_date",current_timestamp())\
                                .withColumn("data_source",lit(v_data_value))\
                                .withColumn("file_date",lit(v_file_date))


# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-3 Drop the unwanted columna
# MAGIC 1.name.forename, name.surname , url 

# COMMAND ----------

#select required columns
results_final_df = results_renamed_df.drop("statusId")


# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-4 Write the file in parquect 

# COMMAND ----------

# MAGIC %md
# MAGIC ####method 1

# COMMAND ----------

# for race_id_list in results_final_df.select("race_id").distinct().collect():
#     if (spark._jsparkSession.catalog().tableExists("formula1_processed.results")):
#         spark.sql(f"ALTER TABLE formula1_processed.results DROP IF EXISTS PARTITION (race_id = {race_id_list.race_id})")

# COMMAND ----------

# results_final_df.write.mode("append").partitionBy('race_id').format("parquet").saveAsTable("formula1_processed.results")

# COMMAND ----------

# MAGIC %md
# MAGIC ####method 2
# MAGIC

# COMMAND ----------

overwrite_partition(results_final_df,'formula1_processed','results','race_id')

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id , count(race_id) from formula1_processed.results  
# MAGIC group by  race_id
# MAGIC order by race_id desc

# COMMAND ----------

# MAGIC %sql
# MAGIC -- DROP TABLE IF EXISTS formula1_processed.results;

# COMMAND ----------

#write the file 
# results_final_df.write.mode("overwrite").parquet(f"{processed_folder_path}/results")

# COMMAND ----------

dbutils.notebook.exit("Success")
