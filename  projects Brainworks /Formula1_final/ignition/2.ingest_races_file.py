# Databricks notebook source
# MAGIC %md
# MAGIC #Ingest racess.csv File

# COMMAND ----------

# MAGIC %md
# MAGIC ####step-1 Read The CSV File Using Spark Datafream Reader

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_data_value","")
v_data_value = dbutils.widgets.get("p_data_value")

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

# Import Functions 
from pyspark.sql.types import StructType,StructField,StringType,IntegerType,DoubleType,TimestampType,DateType
from pyspark.sql.functions import current_timestamp,to_timestamp,concat,col,lit

# COMMAND ----------

# Schema 
races_schema = StructType([StructField("raceId",IntegerType()),
                            StructField("year",IntegerType()),
                            StructField("round",IntegerType()),
                            StructField("circuitId",IntegerType()),
                            StructField("name",StringType()),
                            StructField("date",DateType()),
                            StructField("time",StringType()),
                            StructField("url",StringType()),
                                     
                            ])

# COMMAND ----------

#Read The CSV File Using Spark Datafream Reader
races_df = spark.read.csv(f"{raw_folder_path}/{v_file_date}/races.csv",schema=races_schema,header=True)

# COMMAND ----------

races_df.printSchema()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Rename the columns 

# COMMAND ----------

#Remaining  the Columns
races_renamed_df = races_df.withColumnRenamed("raceId", "race_id") \
                                            .withColumnRenamed("year", "race_year") \
                                            .withColumnRenamed("circuitId", "circuit_id")


# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-3 Add ingestion date to data fream

# COMMAND ----------

races_final_df = (
    races_renamed_df.withColumn(
        "race_timestamp", concat(col("date"), lit(" "), col("time"))
    )
    .withColumn("ingestion_date", current_timestamp())
    .withColumn("data_source", lit(v_data_value))
    .withColumn("file_date", lit(v_file_date))
)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-4 Select only Require columns

# COMMAND ----------

list_column  =['race_id', 'race_year', 'round', 'circuit_id', 'name',"ingestion_date","data_source","file_date","race_timestamp"]

races_selected_df = races_final_df.select(list_column)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-5 Write data to data lake as parquet file

# COMMAND ----------

races_selected_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_processed.races")

# COMMAND ----------

races_selected_df.write.mode("overwrite").parquet(f"{processed_folder_path}/races")

# COMMAND ----------

dbutils.notebook.exit("Success")

# COMMAND ----------

display(spark.read.parquet(f"{processed_folder_path}/races"))
