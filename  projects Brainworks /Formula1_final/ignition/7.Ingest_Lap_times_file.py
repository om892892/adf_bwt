# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest lap_times.json File

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_data_value","")
v_data_value = dbutils.widgets.get("p_data_value")

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

from pyspark.sql.types import StringType,IntegerType,StructType,StructField


# COMMAND ----------

# Get Schema
lap_times_schema = StructType ([  StructField("raceId",IntegerType()),\
                                StructField("driverId",IntegerType()),\
                                StructField("lap",IntegerType()),\
                                StructField("positions",IntegerType()),\
                                StructField("time",StringType()),\
                                StructField("milliseconds",IntegerType()),\
                                
                              
])

# COMMAND ----------

# Read the JSON file with Schema
lap_times_df = spark.read.csv(f"{raw_folder_path}/{v_file_date}/lap_times/lap_times_split*",schema=lap_times_schema)


# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Rename columns and new columns
# MAGIC 1. raceid renamed race_id 
# MAGIC 2. driverId renamed to driver_id
# MAGIC 3. ingestion date added
# MAGIC

# COMMAND ----------

#Rename the column and ingestion 
lap_times_renamed_df = lap_times_df.withColumnRenamed("raceId", 'race_id')\
                                .withColumnRenamed("driverId","driver_id")\
                                .withColumn("ingestion_date",current_timestamp())\
                                    .withColumn("data_source", lit(v_data_value)) \
.withColumn("file_date", lit(v_file_date))


# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-4 Write the file in parquect 

# COMMAND ----------

overwrite_partition(lap_times_renamed_df,'formula1_processed','lap_times','race_id')

# COMMAND ----------

# lap_times_renamed_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_processed.lap_times")

# COMMAND ----------

#write the file 
# lap_times_renamed_df.write.mode("overwrite").parquet(f"{processed_folder_path}/lap_times")

# COMMAND ----------

# display(spark.read.parquet("/mnt/formula1bwt/processed/lap_times"))

# COMMAND ----------

dbutils.notebook.exit("Success")
