# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest qualifying.json File

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_data_value","")
v_data_value = dbutils.widgets.get("p_data_value")

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

from pyspark.sql.types import StringType,IntegerType,StructType,StructField
 

# COMMAND ----------

# Get Schema
qualifying_schema = StructType ([   StructField("qualifyId",IntegerType()),\
                                    StructField("raceId",IntegerType()),\
                                    StructField("driverId",IntegerType()),\
                                    StructField("constructorId",IntegerType()),\
                                    StructField("number",IntegerType()),\
                                    StructField("position",IntegerType()),\
                                    StructField("q1",StringType()),\
                                    StructField("q2",StringType()),\
                                    StructField("q3",StringType()),\
                                                            
    ])

# COMMAND ----------

# Read the JSON file with Schema
qualifying_df = spark.read.json(f"{raw_folder_path}/{v_file_date}/qualifying/qualifying_split*",multiLine=True,schema=qualifying_schema)


# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Rename columns and new columns
# MAGIC 1. qualifyId renamed qualify_id 
# MAGIC 2. raceId renamed to race_id
# MAGIC 3. driverId renamed driver_id 
# MAGIC 4. constructorId renamed constructor_id 
# MAGIC 5. ingestion date added
# MAGIC

# COMMAND ----------

#Rename the column and ingestion 
qualifying_renamed_df = qualifying_df.withColumnRenamed("qualifyId", 'qualify_id')\
                                .withColumnRenamed("raceId","race_id")\
                                .withColumnRenamed("driverId", 'driver_id')\
                                .withColumnRenamed("constructorId","constructor_id")\
                                .withColumn("ingestion_date",current_timestamp())\
                                .withColumn("data_source", lit(v_data_value)) \
.withColumn("file_date", lit(v_file_date))


# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-4 Write the file in parquect 

# COMMAND ----------

overwrite_partition(qualifying_renamed_df,'formula1_processed','qualifying','race_id')

# COMMAND ----------

# qualifying_renamed_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_processed.qualifying")

# COMMAND ----------

#write the file 
# qualifying_renamed_df.write.mode("overwrite").parquet("/mnt/formula1bwt/processed/qualifying")

# COMMAND ----------

dbutils.notebook.exit("Success")
