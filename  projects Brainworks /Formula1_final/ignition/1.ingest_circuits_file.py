# Databricks notebook source
# MAGIC %md
# MAGIC #Ingest Circuits.csv File

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_data_value","")
v_data_value = dbutils.widgets.get("p_data_value")

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

# MAGIC %md
# MAGIC ####step-1 Read The CSV File Using Spark Datafream Reader

# COMMAND ----------

from pyspark.sql.types import StructType,StructField,StringType,IntegerType,DoubleType
from pyspark.sql.functions import lit

# COMMAND ----------

circuit_schema = StructType([StructField("circuitId",IntegerType()),
                            StructField("circuitRef",StringType()),
                            StructField("name",StringType()),
                            StructField("location",StringType()),
                            StructField("country",StringType()),
                            StructField("lat",DoubleType()),
                            StructField("lng",DoubleType()),
                            StructField("alt",IntegerType()),
                            StructField("url",StringType()),
                                     
                            ])

# COMMAND ----------

#Read The CSV File Using Spark Datafream Reader
circuit_df = spark.read.csv(f"{raw_folder_path}/{v_file_date}/circuits.csv",header=True,schema =circuit_schema)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Select only Require columns

# COMMAND ----------

list_column  = ['circuitId', 'circuitRef', 'name', 'location', 'country', 'lat', 'lng', 'alt']

circuits_selected_df = circuit_df.select(list_column)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-3 Rename the columns 

# COMMAND ----------

circuits_renamed_df = circuits_selected_df.withColumnRenamed("circuitId", "circuit_id") \
.withColumnRenamed("circuitRef", "circuit_ref") \
.withColumnRenamed("lat", "latitude") \
.withColumnRenamed("lng", "longitude") \
.withColumnRenamed("alt", "altitude")\
.withColumn("data_source",lit(v_data_value))\
.withColumn("file_date",lit(v_file_date))


# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-4 Add ingestion date to data fream

# COMMAND ----------

circuit_final_df = add_ingestion_date(circuits_renamed_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-5 Write data to data lake as parquet file

# COMMAND ----------

circuit_final_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_processed.circuits")

# COMMAND ----------

circuit_final_df.write.mode("overwrite").parquet(f"{processed_folder_path}/circuits")

# COMMAND ----------

display(spark.read.parquet("/mnt/formulaa1bwt/processed/circuits"))

# COMMAND ----------

dbutils.notebook.exit("Success")
