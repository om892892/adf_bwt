# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest pit_stopss.json File

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_data_value","")
v_data_value = dbutils.widgets.get("p_data_value")

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

from pyspark.sql.types import StringType,IntegerType,DoubleType,StructType,StructField,DateType,FloatType
from pyspark.sql.functions import current_timestamp,concat,col,lit

# COMMAND ----------

# Get Schema
pit_stopss_schema = StructType ([  StructField("driverId",IntegerType()),\
                                StructField("duration",StringType()),\
                                StructField("lap",IntegerType()),\
                                StructField("milliseconds",IntegerType()),\
                                StructField("raceId",IntegerType()),\
                                StructField("stop",StringType()),\
                                StructField("time",StringType()),\
                              
])

# COMMAND ----------

# Read the JSON file with Schema
pit_stopss_df = spark.read.json(f"{raw_folder_path}/{v_file_date}/pit_stops.json",multiLine=True,schema=pit_stopss_schema)

# COMMAND ----------

pit_stopss_df.printSchema()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Rename columns and new columns
# MAGIC 1. raceid renamed race_id 
# MAGIC 2. driverId renamed to driver_id
# MAGIC 3. ingestion date added
# MAGIC

# COMMAND ----------

#Rename the column and ingestion 
pit_stopss_renamed_df = pit_stopss_df.withColumnRenamed("raceId", 'race_id')\
                                .withColumnRenamed("driverId","driver_id")\
                                .withColumn("ingestion_date",current_timestamp())\
                                .withColumn("data_source", lit(v_data_value)) \
.withColumn("file_date", lit(v_file_date)) 


# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-4 Write the file in parquect 

# COMMAND ----------

overwrite_partition(pit_stopss_renamed_df,'formula1_processed','pit_stops','race_id')

# COMMAND ----------

# pit_stopss_renamed_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_processed.pit_stops")

# COMMAND ----------

#write the file 
# pit_stopss_renamed_df.write.mode("overwrite").parquet(f"{processed_folder_path}/pit_stops")

# COMMAND ----------

dbutils.notebook.exit("Success")
