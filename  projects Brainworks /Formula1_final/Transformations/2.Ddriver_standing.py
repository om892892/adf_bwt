# Databricks notebook source
# MAGIC %md
# MAGIC ##produce Driver standing 

# COMMAND ----------

# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_file_date","2021-03-28")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC  %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

# MAGIC %md
# MAGIC ### Find Race Year for which the data to be reprocessed 

# COMMAND ----------

v_file_date

# COMMAND ----------

race_result_list = spark.read.parquet(f"{presentation_folder_path}/race_results").filter(f"file_date = '{v_file_date}'").select("race_year").distinct().collect()

# COMMAND ----------

result_list =[]
for year in race_result_list:
    result_list.append(year.race_year)
print(result_list)

# COMMAND ----------

race_result_df = spark.read.parquet(f"{presentation_folder_path}/race_results").filter(col("race_year").isin(result_list))

# COMMAND ----------

# from pyspark.sql.functions import sum,col,count,when,desc,rank
# from pyspark.sql.window import Window

# COMMAND ----------

from pyspark.sql.functions import sum, count, col, when

driver_standng_df = (
    race_result_df.withColumn("points", col("points").cast("integer"))
    .groupBy("race_year", "driver_name",  "team")
    .agg(
        sum(col("points")).alias("total_point"),
        count(when(col("position") == 1, True)).alias("wins")
    )
)
driver_standng_df.display()

# COMMAND ----------

driver_standng_df.filter("race_year == 2020").display()

# COMMAND ----------

window_spase = Window.partitionBy("race_year").orderBy(desc("total_point"),desc("wins"))
final_df1 = driver_standng_df.withColumn("rank",rank().over(window_spase))
final_df1.display()


# COMMAND ----------

overwrite_partition(final_df1,'formula1_presentation','driver_standing','race_year')

# COMMAND ----------

# display(final_df.filter("race_year == 2020"))

# COMMAND ----------

# final_df.printSchema()

# COMMAND ----------

from pyspark.sql.functions import col

# Cast 'race_year' column to int
# final_df = final_df.withColumn("race_year", col("race_year").cast("string"))

# Cast 'rank' column to int
# final_df = final_df.withColumn("rank", col("rank").cast("int"))

# Write the DataFrame to the table
# overwrite_partition(final_df1, 'formula1_presentation', 'driver_standing', "race_year")

# display(spark.read.parquet())

# COMMAND ----------

# overwrite_partition(final_df,'formula1_presentation','driver_standing','race_year')

# COMMAND ----------



# COMMAND ----------

# final_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_presentation.driver_standing")


# COMMAND ----------

# final_df.write.mode("overwrite").parquet(f"{presentation_folder_path}/driver_standing")

# COMMAND ----------


