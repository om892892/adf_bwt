# Databricks notebook source
# Creates a text input widget with a given name and default value
dbutils.widgets.text("p_file_date","2021-03-28")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

races_df = spark.read.parquet(f"{processed_folder_path}/races").withColumnRenamed("name","race_name").withColumnRenamed("race_timestamp","race_date")

# COMMAND ----------

circuits_df = spark.read.parquet(f"{processed_folder_path}/circuits").withColumnRenamed("location","circuit_location")


# COMMAND ----------

driver_df = spark.read.parquet(f"{processed_folder_path}/drivers").withColumnRenamed("number","driver_number").withColumnRenamed("name","driver_name").withColumnRenamed("nationality","driver_nationality")



# COMMAND ----------

constructors_df = spark.read.parquet(f"{processed_folder_path}/constructors").withColumnRenamed("name","team")


# COMMAND ----------

result_df = spark.read.parquet(f"{processed_folder_path}/results").filter(f"file_date = '{v_file_date}'").withColumnRenamed("time","race_time").withColumnRenamed("race_id","result_race_id").withColumnRenamed("file_date","result_file_date")


# COMMAND ----------

# MAGIC %md
# MAGIC ##Join all tables

# COMMAND ----------

race_circuits_df = races_df.join(circuits_df,races_df.circuit_id == circuits_df.circuit_id,"inner").select("race_id","race_year","race_name","race_date","circuit_location")

# COMMAND ----------

race_join_df = result_df.join(race_circuits_df,result_df.result_race_id == race_circuits_df.race_id)\
                              .join(driver_df,driver_df.driver_id == result_df.driver_id)\
                              .join(constructors_df,constructors_df.constructor_id == result_df.constructor_id)\
                                
            

# COMMAND ----------

final_df = race_join_df.select("race_id","race_year","race_name","race_date","circuit_location","driver_name","driver_number","driver_nationality","team","grid","fastest_lap","race_time","points","position","result_file_date").withColumn("created_date",current_timestamp()).withColumnRenamed("result_file_date","file_date")

# COMMAND ----------

overwrite_partition(final_df,'formula1_presentation','race_results','race_id')

# COMMAND ----------

# MAGIC %sql
# MAGIC --drop table formula1_presentation.race_results

# COMMAND ----------

v_file_date

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from  formula1_presentation.race_results
# MAGIC where race_year =2021;

# COMMAND ----------

# final_df.filter("race_year == 2020 and race_name == 'Abu Dhabi Grand Prix'").orderBy(col("points").desc())

# COMMAND ----------

# race_final_df = final_df.drop_duplicates(["driver_name"]).orderBy(col("points").desc())

# COMMAND ----------

# race_final_df.select(col("driver_name").alias("driver"),
                    #  col("driver_number").alias("number"),
#                      col("team").alias("Team"),
#                      col("grid").alias("Grid"),
#                      col("fastest_lap").alias("FastestLap"),
#                      col("race_time").alias("RaceTime"),
#                      col("points").alias("Points")
#                      ).orderBy(col("points").desc()).display()

# COMMAND ----------

final_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_presentation.race_results")

# COMMAND ----------

final_df.write.mode("overwrite").parquet(f"{presentation_folder_path}/race_results")

# COMMAND ----------

# dbutils.fs.rm("dbfs:/mnt/formula1bwt/presentation/race_results", recurse=True)

# COMMAND ----------



# COMMAND ----------

# output_column_df.write.mode("overwrite").partitionBy(partitioncol_name).format("parquet").saveAsTable(f"{db_name}.{table_name}", path="dbfs:/mnt/new_location/race_results")
