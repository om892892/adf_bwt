# Databricks notebook source
# MAGIC %md
# MAGIC ##produce constructor standing 

# COMMAND ----------

# MAGIC  %run "../includes/configuration"

# COMMAND ----------

# MAGIC %run "../includes/common_function/"

# COMMAND ----------

race_result_df = spark.read.parquet(f"{presentation_folder_path}/race_results")

# COMMAND ----------

from pyspark.sql.functions import sum,col,count,when,desc,rank
from pyspark.sql.window import Window

# COMMAND ----------

constructor_standng_df = race_result_df.withColumn("points",col("points").cast("int")).groupBy("race_year","team").agg(sum("points").alias("total_point"),count(when(col("position")==1,True)).alias("wins"))

# COMMAND ----------

constructor_standng_df1 = constructor_standng_df.filter("race_year == 2020")

# COMMAND ----------

window_spase = Window.partitionBy("race_year").orderBy(desc("total_point"),desc("wins"))
final_df = constructor_standng_df1.withColumn("rank",rank().over(window_spase))


# COMMAND ----------

constructor_final_df=final_df.filter("race_year == 2020")

# COMMAND ----------

final_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_presentation.constructor_standing")


# COMMAND ----------

final_df.write.mode("overwrite").parquet(f"{presentation_folder_path}/constructor_standing")

# COMMAND ----------

display(spark.read.parquet(f"{presentation_folder_path}/constructor_standing"))
