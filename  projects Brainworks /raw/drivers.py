# Databricks notebook source
# MAGIC %run "../set_up/common_function_date"

# COMMAND ----------

from pyspark.sql.functions import col,count,when,sum,avg

# COMMAND ----------

dbutils.widgets.text('race_yr','2024',"enter_the_race_yr_for_driver_standind")
race_year = dbutils.widgets.get('race_yr')

# COMMAND ----------

driver_df = spark.read.parquet(f"{sliver_folder_path}/drivers/").withColumnRenamed("number","driver_number").withColumnRenamed("name","driver_name").withColumnRenamed("nationality","driver_nationality")

# COMMAND ----------

constructors_df = spark.read.parquet(f"{sliver_folder_path}/constructors/*").withColumnRenamed("name","team")


# COMMAND ----------

pit_stop_df = spark.read.parquet(f"{sliver_folder_path}/pitstops/*").withColumnRenamed('race_name','race_names')

# COMMAND ----------

result_df = (
    spark.read.parquet(f"{sliver_folder_path}/results/*")
    .withColumn("points", col("points").cast("int"))
    .withColumn("result_position", col("result_position").cast("int"))
    .withColumnRenamed("season_id", "season_yr")
)

# COMMAND ----------

# MAGIC %md
# MAGIC ##Join all tables

# COMMAND ----------

# race_circuits_df = races_df.join(circuits_df,races_df.circuit_id == circuits_df.circuit_ref,"inner")

# COMMAND ----------

race_join_df =result_df.join(constructors_df, constructors_df.constructor_ref == result_df.constructor_id)\
    .join(pit_stop_df, result_df.race_date == pit_stop_df.race_dt)\
        .join(driver_df, driver_df.driver_id == result_df.driver_id).display()

# COMMAND ----------

race_col_df = race_join_df.select("driver_name","driver_nationality",col("permanentNumber"), "team", "grid", "pit_stop","fastest_lap_detail","race_time","points","race_names","season_yr","race_date","result_position")

# COMMAND ----------

race_col_df.groupBy("season_yr").agg(count("season_yr")).display()

# COMMAND ----------

df_result1 =race_col_df.drop_duplicates(['race_time']).groupBy("season_yr", "driver_name", "driver_nationality").agg(
        sum(col("points")).alias("total_pts"),
        count(when(col("result_position") == 1, True)).alias("wins"))


# COMMAND ----------

from pyspark.sql.window import Window
from pyspark.sql.functions import desc,dense_rank

# COMMAND ----------

windowsp = Window.partitionBy("season_yr").orderBy(desc('total_pts'),desc('wins'))
driver_standing = df_result1.withColumn("rank",dense_rank().over(windowsp))


# COMMAND ----------

race_final_df = driver_standing.filter(col("season_yr") == race_year).display()




# COMMAND ----------

# Driver	Number	Team	Grid	Pits	Fastest Lap	Race Time	Point
race_final_df.select(col("driver_name").alias("driver"),
                     col("permanentNumber").alias("number"),
                     col("team").alias("Team"),
                     col("grid").alias("Grid"),
                     col("fastest_lap_detail").alias("FastestLap"),
                     col("race_time").alias("RaceTime"),
                     col("pit_stop"),
                     col("points").alias("Points")

                     ).orderBy(col("points").desc()).display()

# COMMAND ----------

# final_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_presentation.race_results")

# COMMAND ----------

# final_df.write.mode("overwrite").parquet(f"{gold_folder_path}/race_results")

# COMMAND ----------


