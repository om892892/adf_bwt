# Databricks notebook source
# MAGIC %run ../Utils/Common_functions

# COMMAND ----------

# Get Schema
pit_stopss_schema = StructType ([  StructField("driverId",IntegerType()),\
                                StructField("duration",StringType()),\
                                StructField("lap",IntegerType()),\
                                StructField("milliseconds",IntegerType()),\
                                StructField("raceId",IntegerType()),\
                                StructField("stop",StringType()),\
                                StructField("time",StringType()),\
                              
])

# COMMAND ----------

# Read the JSON file with Schema
pit_stopss_df = spark.read.json(f"/mnt/formulaoneraceadlsgen2/bronze/Pit_stops/20240419/")

# COMMAND ----------

pit_stopss_df = flatten(pit_stopss_df)

# COMMAND ----------

pit_stopss_df.display()

# COMMAND ----------

#Renaming the column and adding Ingestion date columns with dataframe 
pit_stopss_renamed_df = pit_stopss_df.withColumnRenamed("MRData_RaceTable_Races_date", 'race_date')\
                                .withColumnRenamed("MRData_RaceTable_Races_PitStops_stop","stops")\
                                    .withColumnRenamed("MRData_RaceTable_season","Season_id")\
                                        .withColumnRenamed("MRData_RaceTable_Races_raceName","race_name")
                                


# COMMAND ----------

pit_stopss_renamed_df1 = add_ingestion_date(pit_stopss_renamed_df)

# COMMAND ----------

pit_stopss_renamed_df1 = pit_stopss_renamed_df1.select("race_date","Season_id","stops","race_name")

# COMMAND ----------

# writing the dataframe into storage in parquet file format 
pit_stopss_renamed_df1.write.mode("overwrite").parquet(f"/mnt/formulaoneraceadlsgen2/silver/pit_stops")

# COMMAND ----------

display(spark.read.parquet("/mnt/formulaoneraceadlsgen2/silver/pit_stops/"))
