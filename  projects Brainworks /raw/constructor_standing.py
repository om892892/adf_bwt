# Databricks notebook source
from pyspark.sql.functions import col,max,dense_rank,desc,regexp_replace,sum,min,row_number,rank,lit,trim,count,when
from pyspark.sql.window import Window

# COMMAND ----------

dbutils.widgets.text('race_yr','2024',"enter_the_race_yr_for_driver_standind")
race_year = dbutils.widgets.get('race_yr')

# COMMAND ----------

df_result = spark.read.format("delta").load("/mnt/formulaproject/adf-formula1-project/silver/results/20240411")
df_pitstops = spark.read.format("delta").load("/mnt/formulaproject/adf-formula1-project/silver/pitstops/20240411")
df_drivers = spark.read.format("delta").load("/mnt/formulaproject/adf-formula1-project/silver/drivers/20240411")
df_constructors = spark.read.format("delta").load("/mnt/formulaproject/adf-formula1-project/silver/constructors/20240411")

# COMMAND ----------

df_result = df_result.withColumn('points',col("points").cast("int")).withColumn('position',col("position").cast("int"))


# COMMAND ----------

df_result = df_result.withColumnRenamed("time",'race_time').withColumnRenamed('season',"race_yr")
df_drivers = df_drivers.withColumnRenamed("nationality",'drivers_nationality')

# COMMAND ----------

df_result1 = (
    df_result.join(df_pitstops, "race_dt", "left")
    .join(df_drivers, "driver_ref", "left")
    .join(
        df_constructors,
        df_result.constructor_id == df_constructors.constructor_ref,
        "left",
    )
)

# COMMAND ----------

df_result1 = df_result1.select(
    col("full_nm").alias("driver"),
    "drivers_nationality",
    "number",
    col("name").alias("Team"),
    "grid",
    "pit_stop",
    col("fastestLap_time").alias("fastestlap"),
    col("race_time"),
    "points",
    "race_name",
    "race_yr",
    "position",
).distinct()

# COMMAND ----------

df_result1 = df_result1.drop_duplicates(['race_time']).groupBy('race_yr','Team').agg(sum(col("points")).alias("total_pts"),count(when(col('position')==1,True)).alias('wins'))

# COMMAND ----------

windowsp = Window.partitionBy("race_yr").orderBy(desc('total_pts'),desc('wins'))
driver_standing = df_result1.withColumn("rank",dense_rank().over(windowsp))

# COMMAND ----------

driver_standing.filter(col("race_yr")==race_year).display()

# COMMAND ----------


