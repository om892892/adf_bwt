# Databricks notebook source
# MAGIC %run "../set_up/common_function_date"

# COMMAND ----------

from pyspark.sql.functions import col,count

# COMMAND ----------

# %run "../transformation/1.race_result/"

# COMMAND ----------

dbutils.widgets.text('race_yr','2024',"enter_the_race_yr_for_driver_standind")
race_year = dbutils.widgets.get('race_yr')

# COMMAND ----------

races_df = spark.read.parquet(f"{sliver_folder_path}/races/*").withColumnRenamed("name","race_name").withColumnRenamed("race_timestamp","race_date")
# races_df.display()


# COMMAND ----------

circuits_df = spark.read.parquet(f"{sliver_folder_path}/circuits/*").withColumnRenamed("location","circuit_location")
# circuits_df.display()

# COMMAND ----------

driver_df = spark.read.parquet(f"{sliver_folder_path}/drivers/*").withColumnRenamed("number","driver_number").withColumnRenamed("name","driver_name").withColumnRenamed("nationality","driver_nationality")



# COMMAND ----------

pit_stop_df = spark.read.parquet(f"{sliver_folder_path}/pitstops/*").withColumnRenamed('race_name','race_names')
# pit_stop_df.display()

# COMMAND ----------

constructors_df = spark.read.parquet(f"{sliver_folder_path}/constructors/*").withColumnRenamed("name","team")


# COMMAND ----------

result_df = (
    spark.read.parquet(f"{sliver_folder_path}/results/*")
    .withColumnRenamed("race_time", "race_times")
    .withColumnRenamed("race_date", "race_dates")
    .withColumn("result_position", col("result_position").cast("int"))
    .withColumn("points", col("points").cast("int")).withColumnRenamed("season_id", "season_yr")
)
# result_df.orderBy(col("points").desc())
# result_df.display()

# COMMAND ----------

driver_join_df = result_df.join(constructors_df,result_df.constructor_id == constructors_df.constructor_ref,"left")\
            .join(driver_df, driver_df.driver_id == result_df.driver_id)
            # .join(pit_stop_df,result_df.race_dates == pit_stop_df.race_dt,"left")\


# driver_join_df.display()

# COMMAND ----------

# MAGIC %md
# MAGIC ##Join all tables

# COMMAND ----------

# race_circuits_df = races_df.join(circuits_df,races_df.circuit_id == circuits_df.circuit_ref,"inner")

# COMMAND ----------

# race_join_df = (
#     result_df
#     # .join(driver_df, driver_df.driver_id == result_df.driver_id)
#     .join(constructors_df, constructors_df.constructor_ref == result_df.constructor_id)
#     .join(pit_stop_df,result_df.race_dates == pit_stop_df.race_dt)
#     # .join(race_circuits_df, result_df.circuit_id == race_circuits_df.circuit_ref)
# )

# race_join_df.display()

# COMMAND ----------

# race_col_df = driver_join_df.select("driver_name","driver_nationality",col("permanentNumber"), "team", "grid", "pit_stop","fastest_lap_detail","race_times","points","race_names","season_yr","race_dates","result_position")

# COMMAND ----------

from pyspark.sql.functions import col,count,when,sum,avg,desc,dense_rank
from pyspark.sql.window import Window


# COMMAND ----------

df_result1 =driver_join_df.groupBy("season_yr", "driver_name", "team").agg(
        sum(col("points")).alias("total_pts"),
        count(when(col("result_position") == 1, True)).alias("wins"))
        # .display()


# COMMAND ----------

windowsp = Window.partitionBy("season_yr").orderBy(desc('total_pts'),desc('wins'))
driver_standing = df_result1.withColumn("rank",dense_rank().over(windowsp))
# .display()


# COMMAND ----------

race_final_df = driver_standing.filter(col("season_yr") == race_year).display()


# COMMAND ----------

# final_df = race_join_df.select(
#     "seuason_i",
#     "race_name",
#     "race_date",
#     "circuit_location",
#     col("name"),
#     "driver_name",
#     col("permanentNumber"),
#     "driver_nationality",
#     "team",
#     "grid",
#     "fastest_lap_detail",
#     "pit_stop",
#     "race_time",
#     "points",
#     "result_position",
# )


# COMMAND ----------

# # Driver	Number	Team	Grid	Pits	Fastest Lap	Race Time	Point
# race_final_df.select(col("driver_name").alias("driver"),
#                      col("permanentNumber").alias("number"),
#                      col("team").alias("Team"),
#                      col("grid").alias("Grid"),
#                      col("fastest_lap_detail").alias("FastestLap"),
#                      col("race_time").alias("RaceTime"),
#                      col("pit_stop"),
#                      col("points").alias("Points")

#                      ).orderBy(col("points").desc()).display()

# COMMAND ----------

# final_df.write.mode("overwrite").parquet(f"{gold_folder_path}/race_results")
