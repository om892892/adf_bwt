# Databricks notebook source
# MAGIC %run "../set_up/common_function_date"

# COMMAND ----------

from pyspark.sql.functions import col,count

# COMMAND ----------

dbutils.widgets.text('race_yr','2024',"enter_the_race_yr_for_driver_standind")
race_year = dbutils.widgets.get('race_yr')

# COMMAND ----------

# races_df = spark.read.parquet(f"{sliver_folder_path}/races/*").withColumnRenamed("name","race_name").withColumnRenamed("race_timestamp","race_date")
# races_df.display()


# COMMAND ----------

# circuits_df = spark.read.parquet(f"{sliver_folder_path}/circuits/*").withColumnRenamed("location","circuit_location")
# circuits_df.display()

# COMMAND ----------

driver_df = spark.read.parquet(f"{sliver_folder_path}/drivers/*").withColumnRenamed("number","driver_number").withColumnRenamed("name","driver_name").withColumnRenamed("nationality","driver_nationality")
# driver_df.dispaly()

# COMMAND ----------

# pit_stop_df = spark.read.parquet(f"{sliver_folder_path}/pitstops/*").withColumnRenamed('race_name','race_names')
# pit_stop_df.display()

# COMMAND ----------

constructors_df = spark.read.parquet(f"{sliver_folder_path}/constructors/*").withColumnRenamed("name","team")


# COMMAND ----------

result_df = (
    spark.read.parquet(f"{sliver_folder_path}/results/*")
    .withColumnRenamed("race_time", "race_times")
    .withColumnRenamed("race_date", "race_dates")
    .withColumn("result_position", col("result_position").cast("int"))
    .withColumn("points", col("points").cast("int")).withColumnRenamed("season_id", "season_yr")
)


# COMMAND ----------

driver_join_df = result_df.join(constructors_df,result_df.constructor_id == constructors_df.constructor_ref,"left")\
            .join(driver_df, driver_df.driver_id == result_df.driver_id)
            # .join(pit_stop_df,result_df.race_dates == pit_stop_df.race_dt,"left")\


# driver_join_df.display()

# COMMAND ----------

# MAGIC %md
# MAGIC ##Join all tables

# COMMAND ----------

from pyspark.sql.functions import col,count,when,sum,avg,desc,dense_rank,max
from pyspark.sql.window import Window


# COMMAND ----------

df_result1 =driver_join_df.groupBy("season_yr", "driver_name", "team").agg(
        sum(col("points")).alias("total_pts"),
        count(when(col("result_position") == 1, True)).alias("wins")).orderBy(col("total_pts").desc())
        # .display()


# COMMAND ----------

# windowsp = Window.partitionBy("season_yr").orderBy(desc('total_pts'),desc('wins'))
# driver_standing = df_result1.withColumn("rank",dense_rank().over(windowsp))
# # .display()


# COMMAND ----------

race_final_df = df_result1.filter(col("season_yr") == race_year).display()

