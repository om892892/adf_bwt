# Databricks notebook source
# MAGIC %run "../set_up/common_function_date"

# COMMAND ----------

from pyspark.sql.functions import col, count

# COMMAND ----------

races_df = (
    spark.read.parquet(f"{sliver_folder_path}/races/*")
    .withColumnRenamed("name", "race_name")
    .withColumnRenamed("race_timestamp", "race_date")
    .withColumnRenamed("race_time", "race_times")
)
# races_df.display()

# COMMAND ----------

circuits_df = spark.read.parquet(f"{sliver_folder_path}/circuits/*").withColumnRenamed(
    "location", "circuit_location"
)
# circuits_df.display()

# COMMAND ----------

driver_df = (
    spark.read.parquet(f"{sliver_folder_path}/drivers/*")
    .withColumnRenamed("number", "driver_number")
    .withColumnRenamed("name", "driver_name")
    .withColumnRenamed("nationality", "driver_nationality")
)

# driver_df.display()

# COMMAND ----------

pit_stop_df = spark.read.parquet(f"{sliver_folder_path}/pitstops/*").withColumnRenamed(
    "race_name", "race_names"
)
# pit_stop_df.display()

# COMMAND ----------

constructors_df = spark.read.parquet(
    f"{sliver_folder_path}/constructors/*"
).withColumnRenamed("name", "team")
# constructors_df.display()

# COMMAND ----------

result_df = (
    spark.read.parquet(f"{sliver_folder_path}/results/*")
    .withColumnRenamed("time", "race_time")
    .withColumnRenamed("race_date", "race_dates")
    .withColumn("points", col("points").cast("int"))
)
result_df.orderBy(col("points").desc())
# result_df.dispaly()

# COMMAND ----------

# MAGIC %md
# MAGIC ##Join all tables

# COMMAND ----------

race_circuits_df = races_df.join(
    circuits_df, races_df.circuit_id == circuits_df.circuit_ref, "inner"
)
# race_circuits_df.display()

# COMMAND ----------

race_join_df = (
    result_df.join(
        race_circuits_df, result_df.circuit_id == race_circuits_df.circuit_ref
    )
    .join(driver_df, driver_df.driver_id == result_df.driver_id)
    .join(constructors_df, constructors_df.constructor_ref == result_df.constructor_id)
    .join(pit_stop_df, result_df.race_dates == pit_stop_df.race_dt)
)

# race_join_df.display()

# COMMAND ----------

final_df = race_join_df.select(
    "season_id",
    "race_name",
    "race_date",
    "circuit_location",
    col("name"),
    "driver_name",
    col("permanentNumber"),
    "driver_nationality",
    "team",
    "grid",
    "fastest_lap_detail",
    "pit_stop",
    "race_time",
    "points",
    "result_position",
)

# COMMAND ----------

race_final_df = final_df.filter(
    "season_id == 2020 and race_name == 'Abu Dhabi Grand Prix'"
).orderBy(col("points").desc())

# COMMAND ----------

race_final_df = race_final_df.drop_duplicates(["driver_name"]).orderBy(
    col("points").desc()
)

# COMMAND ----------

# Driver	Number	Team	Grid	Pits	Fastest Lap	Race Time	Point
race_final_df.select(
    col("driver_name").alias("driver"),
    col("permanentNumber").alias("number"),
    col("team").alias("Team"),
    col("grid").alias("Grid"),
    col("pit_stop"),
    col("fastest_lap_detail").alias("FastestLap"),
    col("race_time").alias("RaceTime"),
    col("points").alias("Points"),
).orderBy(col("points").desc()).display()

# COMMAND ----------

# final_df.write.mode("overwrite").format("parquet").saveAsTable("formula1_presentation.race_results")

# COMMAND ----------

final_df.write.mode("overwrite").parquet(f"{gold_folder_path}/race_results")

# COMMAND ----------


