# Databricks notebook source
from pyspark.sql.functions import concat,col,lit,explode

# COMMAND ----------

# Adding ingestion_date

# COMMAND ----------

from pyspark.sql.functions import current_timestamp

def add_ingestion_date(input_df):
    output_df = input_df.withColumn("ingestion_date",current_timestamp())
    return output_df


# COMMAND ----------

#Containers paths 

# COMMAND ----------

bronze_folder_path = '/mnt/formulaa1bwt/bronze/'
sliver_folder_path = '/mnt/formulaa1bwt/sliver/'
gold_folder_path = '/mnt/formulaa1bwt/gold/'

# COMMAND ----------

##Convert MultiLine JSON in to Flatten

# COMMAND ----------

def flatten(df):
    # Flatten struct types
    struct_fields = [field for field in df.schema.fields if str(field.dataType).startswith("StructType")]
    for field in struct_fields:
        nested_fields = [col(f"{field.name}.{subfield.name}").alias(f"{field.name}_{subfield.name}") for subfield in field.dataType.fields]
        df = df.select("*", *nested_fields).drop(field.name)

    # Flatten array types
    array_fields = [field for field in df.schema.fields if str(field.dataType).startswith("ArrayType")]
    for field in array_fields:
        df = df.withColumn(field.name, explode(field.name))

    # Recursively flatten nested structs and arrays
    if struct_fields or array_fields:
        df = flatten(df)

    return df
