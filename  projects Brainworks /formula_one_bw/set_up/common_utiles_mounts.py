# Databricks notebook source
# It's  gave the hard-coded valure of services principal 
# client_id = "54c472b3-fdbc-40b8-b966-9422e13fe417"
# tanant_id = "e5067042-8535-4ad3-a346-6b72b6f740be"
# clinet_secret = "6538Q~iAjjNGfm13pxCw5Kt~KH6Wu2SRa-rnucNi"

# COMMAND ----------

def mount_adls(storage_account_name, container_name ):
    # Get secrets from  key vault    

    # It's  gave the hard-coded valure of services principal 
    client_id = "54c472b3-fdbc-40b8-b966-9422e13fe417"
    tanant_id = "e5067042-8535-4ad3-a346-6b72b6f740be"
    clinet_secret = "6538Q~iAjjNGfm13pxCw5Kt~KH6Wu2SRa-rnucNi"

    # Set spark config
    configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": client_id,
          "fs.azure.account.oauth2.client.secret":clinet_secret,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{tanant_id}/oauth2/token"}
    
    #Unmount the mount point if already exists   
    if any(mount.mountPoint == f"/mmnt/{storage_account_name}/{container_name}" for mount in dbutils.fs.mounts()):
        dbutils.fs.unmount(f"/mnt/{storage_account_name}/{container_name}")
    
    # Optionally, you can add <directory-name> to the source URI of your mount point.
    dbutils.fs.mount(
        source = f"abfss://{container_name}@{storage_account_name}.dfs.core.windows.net/",
        mount_point = f"/mnt/{storage_account_name}/{container_name}",
        extra_configs = configs)
        
    # To cheak the mounts files 
    display(dbutils.fs.mounts())

# COMMAND ----------

mount_adls('formulaa1bwt','bronze')

# COMMAND ----------

mount_adls('formulaa1bwt','sliver')

# COMMAND ----------

mount_adls('formulaa1bwt','gold')
