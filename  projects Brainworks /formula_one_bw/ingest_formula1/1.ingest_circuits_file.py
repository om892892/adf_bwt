# Databricks notebook source
# MAGIC %run "../set_up/common_function_date"

# COMMAND ----------

# MAGIC %md
# MAGIC #Ingest Circuits.csv File

# COMMAND ----------

# MAGIC %md
# MAGIC ####step-1 Read The CSV File Using Spark Datafream Reader

# COMMAND ----------

#import functions 
from pyspark.sql.types import StructType,StructField,StringType,IntegerType,DoubleType
from pyspark.sql.functions import lit

# COMMAND ----------

#write schema
circuit_schema = StructType([StructField("circuitId",IntegerType()),
                            StructField("circuitRef",StringType()),
                            StructField("name",StringType()),
                            StructField("location",StringType()),
                            StructField("country",StringType()),
                            StructField("lat",DoubleType()),
                            StructField("lng",DoubleType()),
                            StructField("alt",IntegerType()),
                            StructField("url",StringType()),
                                     
                            ])

# COMMAND ----------

#Read The CSV File Using Spark Datafream Reader
circuit_df = spark.read.csv(f"{bronze_folder_path}/circuits/*",header=True,schema =circuit_schema)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Rename the columns 

# COMMAND ----------

#rename the column
circuits_renamed_df = circuit_df.withColumnRenamed("circuitId", "circuit_id") \
.withColumnRenamed("circuitRef", "circuit_ref") \
.withColumnRenamed("lat", "latitude") \
.withColumnRenamed("lng", "longitude") \
.withColumnRenamed("alt", "altitude")\



# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-3 Add ingestion date to data fream

# COMMAND ----------

#add ingestion
circuit_final_df = add_ingestion_date(circuits_renamed_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-4 Write data to data lake as parquet file

# COMMAND ----------

circuit_final_df.write.mode("overwrite").parquet(f"{sliver_folder_path}/circuits")

# COMMAND ----------

display(spark.read.parquet(f"{sliver_folder_path}/circuits"))
