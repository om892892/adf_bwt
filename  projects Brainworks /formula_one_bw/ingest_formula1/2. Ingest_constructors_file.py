# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest Constructors.json File

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

# MAGIC %run "../set_up/common_function_date"

# COMMAND ----------

from pyspark.sql.types import StringType,IntegerType,DoubleType,StructType,StructField
from pyspark.sql.functions import current_timestamp,lit

# COMMAND ----------

# Get Schema
constructors_schema = StructType ([StructField("constructorId",IntegerType()),\
                    StructField("constructorRef",StringType()),
                    StructField("name",StringType()),\
                    StructField("nationality",StringType()),\
                    StructField("url",StringType())
])

# COMMAND ----------

# Read the JSON file with Schema
constructors_df = spark.read.json(f"{bronze_folder_path}/constructors/*",schema=constructors_schema)


# COMMAND ----------

# MAGIC %md
# MAGIC ### Rename the column and new column ingestion date

# COMMAND ----------

# Rename the column and ingestion
constructors_renamed_df = (
    constructors_df.withColumnRenamed("constructorId", "constructor_id")
    .withColumnRenamed("constructorRef", "constructor_ref")
    )

# COMMAND ----------

circuit_final_df = add_ingestion_date(constructors_renamed_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ###Write the file in parquect 

# COMMAND ----------

#write the file 
circuit_final_df.write.mode("overwrite").parquet(f"{sliver_folder_path}/constructors")

# COMMAND ----------

display(spark.read.parquet(f"{sliver_folder_path}/constructors"))
