# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest results.json File

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

# MAGIC
# MAGIC %run "../set_up/common_function_date"

# COMMAND ----------

# from pyspark.sql.types import StringType,IntegerType,DoubleType,StructType,StructField,DateType,FloatType
from pyspark.sql.functions import current_timestamp,concat,col,lit,explode

# COMMAND ----------

# # Get Schema
# results_schema = StructType ([  StructField("constructorId",IntegerType()),\
#                                 StructField("driverId",IntegerType()),\
#                                 StructField("fastestLap",IntegerType()),\
#                                 StructField("fastestLapSpeed",FloatType()),\
#                                 StructField("fastestLapTime",StringType()),\
#                                 StructField("grid",IntegerType()),\
#                                 StructField("laps",IntegerType()),\
#                                 StructField("milliseconds",IntegerType()),\
#                                 StructField("number",IntegerType()),\
#                                 StructField("points",FloatType()),\
#                                 StructField("position",IntegerType()),\
#                                 StructField("positionOrder",IntegerType()),\
#                                 StructField("positionText",StringType()),\
#                                 StructField("raceId",IntegerType()),\
#                                 StructField("rank",IntegerType()),\
#                                 StructField("resultId",IntegerType()),\
#                                 StructField("statusId",StringType()),\
#                                 StructField("time",StringType()),\
                                
#                 ])

# COMMAND ----------

results_df = spark.read.json(f"{bronze_folder_path}/results/*")

# COMMAND ----------

# # Read the JSON file with Schema
# results_df = spark.read.json(f"{bronze_folder_path}/results/*")
# # for i in results_df.schema.fields:
# #     # if str(i.dataType).startswith('ArrayType'):
# #     #     print('----->',i)
# #     if str(i.dataType).startswith("StructType"):
# #         print('Struct--->',f"{i.name}.{i.dataType.fields}")

# for i in results_df.schema:
#     if str(i.dataType).startswith("StructType"):
#         nested_list = [col(f"{i.name}.{j.name}") for j in i.dataType.fields]
#         print(nested_list)

# COMMAND ----------

results_df1 = flatten(results_df)

# COMMAND ----------

results_df1.display()

# COMMAND ----------

results_col_df1 = results_df1.select(col("date").alias("race_date"),
    col("season").alias("season_id"),
    col("round").alias("round_id"),
    col("Circuit_circuitId").alias("circuit_id"),
    col("Results_Constructor_constructorId").alias("constructor_id"),
    col("Results_Driver_driverId").alias("driver_id"),
    col("Results_FastestLap_Time_time").alias("fastest_lap_detail"),
    col("time").alias("race_time"),
    col("Results_grid").alias("grid"),
    col("Results_laps").alias("laps"),
    col("Results_position").alias("result_position"),
    col("Results_points").alias("points"),
    col("Results_positionText").alias("position_text")
)

# COMMAND ----------

# results_selected_column_df = results_df.select(
#     col("season").alias("season_id"),
#     col("round").alias("round_id"),
#     col("Circuit")["circuitId"].alias("circuit_id"),
#     col("Results")["Constructor"]["constructorId"][0].alias("constructor_id"),
#     col("Results")["Driver"]["driverId"][0].alias("driver_id"),
#     col("Results")["FastestLap"]["Time"]["time"].alias("fastest_lap_detail"),
#     col("Results")["grid"].alias("grid"),
#     col("Results")["laps"].alias("laps"),
#     col("Results")["position"].alias("result_position"),
#     col("Results")["points"].alias("points"),
#     col("Results")["positionText"].alias("position_text")
# )

# COMMAND ----------

# results_col_df = (
#     results_selected_column_df.withColumn("fastest_lap_detail", explode("fastest_lap_detail"))
#     .withColumn("grid", explode("grid"))
#     .withColumn("laps", explode("laps"))
#     .withColumn("result_position", explode("result_position"))
#     .withColumn("points", explode("points"))
#     .withColumn("position_text", explode("position_text"))
# )

# COMMAND ----------


results_col_df = add_ingestion_date(results_col_df1)

# COMMAND ----------

results_col_df.display()

# COMMAND ----------

final_result_df = results_col_df1.distinct()

# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-4 Write the file in parquect 

# COMMAND ----------

# # write the file 
final_result_df.write.mode("overwrite").parquet(f"{sliver_folder_path}/results")

# COMMAND ----------

display(spark.read.parquet(f"{sliver_folder_path}/results"))
