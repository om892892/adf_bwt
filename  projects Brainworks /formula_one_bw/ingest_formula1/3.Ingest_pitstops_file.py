# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest pit_stopss.json File

# COMMAND ----------

# MAGIC %run "../set_up/common_function_date"

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

from pyspark.sql.types import StringType,IntegerType,DoubleType,StructType,StructField,DateType,FloatType


# COMMAND ----------

# Get Schema
pit_stopss_schema = StructType ([  StructField("driverId",IntegerType()),\
                                StructField("duration",StringType()),\
                                StructField("lap",IntegerType()),\
                                StructField("milliseconds",IntegerType()),\
                                StructField("raceId",IntegerType()),\
                                StructField("stop",StringType()),\
                                StructField("time",StringType()),\
                              
])

# COMMAND ----------

# Read the JSON file with Schema
pit_stopss_df = spark.read.json(f"{bronze_folder_path}/pitstops/*")


# COMMAND ----------

pit_stopss_df1 = flatten(pit_stopss_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Rename columns and new columns
# MAGIC

# COMMAND ----------

pit_stopss_renamed_df = pit_stopss_df1.withColumnRenamed("MRData_RaceTable_Races_date", 'race_date')\
                                .withColumnRenamed("MRData_RaceTable_Races_PitStops_stop","stops")\
                                    .withColumnRenamed("MRData_RaceTable_season","Season_id")\
                                        .withColumnRenamed("MRData_RaceTable_Races_raceName","race_name")

# COMMAND ----------

pit_stopss_renamed_df1 = add_ingestion_date(pit_stopss_renamed_df)

# COMMAND ----------

pit_stopss_renamed_df1 = pit_stopss_renamed_df1.select("race_date","Season_id","stops","race_name")

# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-3 Write the file in parquect 

# COMMAND ----------

#write the file 
pit_stopss_renamed_df1.write.mode("overwrite").parquet(f"{bronze_folder_path}/pit_stops")
