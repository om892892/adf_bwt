# Databricks notebook source
# MAGIC %md
# MAGIC ##Ingest drivers.json File

# COMMAND ----------

# MAGIC
# MAGIC %run "../set_up/common_function_date"

# COMMAND ----------

# MAGIC %md
# MAGIC ####Step-1 Read The JSON File using spark datafram reader 

# COMMAND ----------

# Read the JSON file with Schema
season_df = spark.read.json(f"{bronze_folder_path}/season/*")
season_df.display()

# COMMAND ----------

season_df.display()

# COMMAND ----------

# MAGIC %md
# MAGIC ###Step-4 Write the file in parquect 

# COMMAND ----------

#write the file 
season_df.write.mode("overwrite").parquet(f"{sliver_folder_path}/seasons")

# COMMAND ----------

display(spark.read.parquet(f"{sliver_folder_path}/seasons"))
