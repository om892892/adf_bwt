# Databricks notebook source
# MAGIC %md
# MAGIC #Ingest racess.csv File

# COMMAND ----------

# MAGIC %md
# MAGIC ####step-1 Read The CSV File Using Spark Datafream Reader

# COMMAND ----------

# MAGIC %run "../set_up/common_function_date"

# COMMAND ----------

#Read The CSV File Using Spark Datafream Reader
races_df = spark.read.json(f"{bronze_folder_path}/races/*")
races_df.printSchema()


# COMMAND ----------

races_df.display()

# COMMAND ----------

# race column list :['race_date', 'race_name', 'round', 'season', 'race_time', 'circuit_id']
races_selected_column_df = races_df.select(col("date").alias("race_date"),col("raceName").alias("race_name"),col("round"),col("season"),col("time").alias("race_time"),col("Circuit")["circuitid"].alias("circuit_id"))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-2 Add ingestion date to data fream

# COMMAND ----------

races_final_df = (
    races_selected_column_df.withColumn("ingestion_date", current_timestamp())   
)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step-3 Write data to data lake as parquet file

# COMMAND ----------

races_final_df.write.mode("overwrite").parquet(f"{sliver_folder_path}/races")

# COMMAND ----------

display(spark.read.parquet(f"{sliver_folder_path}/races"))
